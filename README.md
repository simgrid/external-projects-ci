External Projects CI
====================

This project hosts continous integration files for some projects that use SimGrid.
The goal of this project is to determine if pinned versions of some projects still work with up-to-date SimGrid.
