{ kapack ? import
    (fetchTarball "https://github.com/oar-team/nur-kapack/archive/master.tar.gz")
  {}
}:
let
  # The list of packages starts here.
  self = rec {
    pkgs = kapack.pkgs;

    simgrid = kapack.simgrid.override { debug = true; }; # debug build -> fast build
    simgrid-master = simgrid.overrideAttrs (old: rec { # specialization of simgrid
      src = builtins.fetchGit { # use framagit/master as source
        url = "https://framagit.org/simgrid/simgrid.git";
        ref = "master";
      };
      propagatedBuildInputs = old.propagatedBuildInputs ++ [ pkgs.eigen ];
      patches = [];
    });

    # Batsim
    batsim_def = import ./batsim { simgrid = simgrid-master; testVersion = "fixed"; };
    batsim = batsim_def.batsim;
    batsim_tests = batsim_def.integration_tests;
  };
in
  self
